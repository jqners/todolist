const tasks = ["putzen", "kochen", "tanzen"];
printTaskList();

document.getElementById("addTask").addEventListener("click", function(){
    let NewTask = document.getElementById("txtnewtask").value;
    tasks.push(NewTask);
    printTaskList();
});

function printTaskList(){
    document.getElementById("tasklist").innerHTML = getHTMLTasks();
}

function getHTMLTasks(){

    let html ="";
    tasks.forEach(element => {
        html += "<li>" + element + "</li>";
        
    });

    return html;
}